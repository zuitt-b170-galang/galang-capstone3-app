import { Fragment ,useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext'

export default function AppNavbar(){

  const { user, unsetUser } = useContext(UserContext);
  const navigate = useNavigate();

  const logout = () => {
    unsetUser();
    navigate('/login');
  }

  let rightNav = (user.access === null) ? (
      <Fragment>
        <Nav.Link as={NavLink} to='/register'>Register</Nav.Link>
        <Nav.Link as={NavLink} to='/login'>Login</Nav.Link>
      </Fragment>
    ) : (
      <Fragment>
        <Nav.Link onClick={logout}>Logout</Nav.Link>
      </Fragment>
    )

  return(
      <Navbar bg='danger' expand='lg'>
        <Navbar.Brand as={Link} to={"/"} className="text-light font-weight-bold">KSeoulMerch</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
                <Nav.Link as={NavLink} to='/'>Home</Nav.Link>
                <Nav.Link as={NavLink} to='/products'>Products</Nav.Link>
                <Nav.Link as={NavLink} to='/cart'>Cart</Nav.Link>
                {(user.id !== null) ? 
                            <Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>
                        : 
                            <Fragment>
                                <Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
                                <Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
                            </Fragment>
                    }
            </Nav>
            <Nav className = 'ml-auto'>
              {rightNav}
            </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
}
