import React from 'react';
import { Card } from 'react-bootstrap';

export default function OrderCard(props) {

	let order = props.order;
	const {totalAmount,purchasedOn}=order;
 	
			    return (
			        <Card>
			            <Card.Body>
			                <Card.Title>PhP {order.totalAmount}</Card.Title>
			                <Card.Subtitle>Total Amount:</Card.Subtitle>
			                <Card.Text>{order.purchasedOn}</Card.Text>
			                <Card.Subtitle>Purchaed On:</Card.Subtitle>
			            </Card.Body>
			        </Card>
			    )
			}

