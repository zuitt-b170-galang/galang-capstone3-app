// import Button from 'react-bootstrap/Button';
			// Bootstrap grid system components
			// import Row from 'react-bootstrap/Row';
			// import Col from 'react-bootstrap/Col';
			// import { Row } from 'react-bootstrap';
			// import { Col } from 'react-bootstrap';
			import { Jumbotron, Button, Row, Col } from 'react-bootstrap'
			import Nav from 'react-bootstrap/Nav';
			import { Link, NavLink } from 'react-router-dom';

export default function Banner() {
				return (
					<Jumbotron className="bg-primary">
					<Row>
	                	<Col className="p-5 text-center text-light">
		                    <h1>KSeoulMerch</h1>
		                    <p>one stop shop for kpop fans</p>
		                    <Nav.Link as={NavLink} to="/products" exact><Button variant="danger" className="my-auto" >View All Products</Button></Nav.Link>
		                </Col>
	                </Row>
	                </Jumbotron>
				)
			}