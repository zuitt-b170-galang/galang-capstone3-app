import React, { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';

export default function ProductCard(props) {

	let product = props.product;
	const {name,description,price}=product;

	const [ isDisabled, setIsDisabled ] = useState(0);
    const [ stocks, setStocks ] = useState(30);

    useEffect(()=> {
        if (stocks === 0) {
            setIsDisabled(true);
        }
    },[stocks]);
			    return (
			        <Card>
			            <Card.Body>
			                <Card.Title>{product.name}</Card.Title>
			                <Card.Subtitle>Description:</Card.Subtitle>
			                <Card.Text>{product.description}</Card.Text>
			                <Card.Subtitle>Price:</Card.Subtitle>
			                <Card.Text>PhP {product.price}</Card.Text>
			                <Button variant="primary" onClick={()=>setStocks(stocks-1)} disabled={isDisabled}>Add to Cart</Button>
			            </Card.Body>
			        </Card>
			    )
			}

