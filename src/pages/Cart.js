import { Fragment, useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import OrderCard from "../components/OrderCard"


export default function Cart(ordersData){

	const { user } = useContext(UserContext);
    const [orders, setOrders] = useState([]);

	const viewCart = () => {
        fetch(`https://mighty-stream-66738.herokuapp.com/api/users/getorders`)
        .then(res => res.json())
        .then(data => {
            setOrders(data);
        });

    }

    useEffect(() => {
        const ordersArr = ordersData.map(order => {
        	if(order.isActive === true){
				return (
					<OrderCard orderProp={order} key={order._id}/>
				)
        	}else{
        		return null;
        	}
        });
        setOrders(ordersArr);
    }, [ordersData]);

    return(
        <Fragment>
            {orders}
        </Fragment>
    );

}