
import	React, { useState, useEffect, useContext } from 'react';
import	{ Navigate, useNavigate } from 'react-router-dom';

import UserContext from '../UserContext.js';


import { Form, Container, Button } from 'react-bootstrap';
import Swal from 'sweetalert2'; //npm install sweetalert2 to install sweetalert2

export default function Register(){

	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);	


function registerUser(e) {
	// Prevents page redirection via form submission
	e.preventDefault();
	fetch('https://mighty-stream-66738.herokuapp.com/api/users/register', {
	    		method: "POST",
	    		headers: {
	    		    'Content-Type': 'application/json'
	    		},
	    		body: JSON.stringify({
	    		    firstName: firstName,
	    		    lastName: lastName,
	    		    email: email,
	    		    password: password1
	    		})

	    	})
	    	.then(res => res.json())
	    	.then(data => {
	    		console.log(data);

	    		if (data === true) {

	    		    setFirstName('');
	    		    setLastName('');
	    		    setEmail('');
	    		    setPassword1('');
	    		    setPassword2('');

	    		    Swal.fire({
	    		        title: 'Registration successful',
	    		        icon: 'success',
	    		    });

	    		    navigate("/login");

	    		} else {

	    		    Swal.fire({
	    		        title: 'Something wrong',
	    		        icon: 'error',
	    		        text: 'Please try again.'   
	    		    });

	    		}
	    	})

}

	useEffect(() => {
		if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [firstName, lastName, email, password1, password2])

	return (
		(user.access !== null) ?
		    <Navigate to="/courses" />
		:
		<Container>
			<h1>Register</h1>
			<Form className="mt-3" onSubmit={(e) => registerUser(e)}>
				<Form.Group controlId="firstName">
				    <Form.Label>First Name</Form.Label>
				    <Form.Control 
				        type="text" 
				        placeholder="Enter first name"
				        value={firstName} 
				        onChange={e => setFirstName(e.target.value)}
				        required
				    />
				</Form.Group>

				<Form.Group controlId="lastName">
				    <Form.Label>Last Name</Form.Label>
				    <Form.Control 
				        type="text" 
				        placeholder="Enter last name"
				        value={lastName} 
				        onChange={e => setLastName(e.target.value)}
				        required
				    />
				</Form.Group>

			  <Form.Group className="mb-3" controlId="userEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter email" 
			    	value = {email}
			    	onChange = { e => setEmail(e.target.value)}
			    	required 
			    />
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>
			  </Form.Group>


			  <Form.Group className="mb-3" controlId="password1">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Password" 
			    	value={password1}
			    	onChange = { e => setPassword1(e.target.value)}
			    	required 
			    />
			  </Form.Group>
			  
			  <Form.Group className="mb-3" controlId="password2">
			    <Form.Label>Verify Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Verify Password" 
			    	value={password2}
			    	onChange = { e => setPassword2(e.target.value)}
			    	required 
			    />

			  </Form.Group>

			  {(isActive) ? 
			  		<Button variant="primary" type="submit" id="submitBtn">
			  		  Submit
			  		</Button>
			  	:
				  	<Button variant="primary" type="submit" id="submitBtn" disabled>
				  	  Submit
				  	</Button>
			  }
			  
			</Form>
		</Container>
	)
}


