import { useContext, useEffect } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import UserContext from '../UserContext';
import Login from './Login';

export default function Logout() {


    const { unsetUser, setUser } = useContext(UserContext);

    unsetUser();

    useEffect(() => {
        setUser({id: null});
    })
    return (
        <Route path = "/login" element = {<Login />} />
    )

}