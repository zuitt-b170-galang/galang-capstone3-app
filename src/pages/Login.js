import React, { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext'

import { Form, Container, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Login() {

    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [ isDisabled, setIsDisabled ] = useState(false);
    const [isActive, setIsActive] = useState(true);

    useEffect(() => {
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    }, [email, password]); 

    function authenticate(e) {
        e.preventDefault();

        fetch('https://mighty-stream-66738.herokuapp.com/api/users/login', {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify( {
                email: email, 
                password: password
                } )
        })
        .then((response) => response.json())
        .then(data => {
            if (typeof data.access !== "undefined") {
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);
                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                });
            } else {
                Swal.fire({
                    title: "Login failed",
                    icon: "error",
                    text: "Try again."
                });
            };
        })
        setEmail('');
        setPassword('');
    }

    const retrieveUserDetails = (token) => {

        fetch(`https://mighty-stream-66738.herokuapp.com/api/users/details`, {
            headers: {
                Authorization: `Bearer ${ token }`
            }
        })
        .then(res => res.json())
        .then(data => {
            setUser({
              id: data._id,
              isAdmin: data.isAdmin
            });
        })
    };

      
    if (user.access !== null) {

        return <Navigate replace to="/" />
    }

    return (
        <Container fluid>
            <h3>Login</h3>
            <Form onSubmit={(e) => authenticate(e)}>
                <Form.Group>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
                </Form.Group>
                <Button variant="success" type="submit" disabled={isDisabled}>Login</Button>
            </Form>
        </Container>
    )
}
