import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import ksm001 from '../products/ksm001'

const productsData = [
		    {
		        id: "ksm001",
		        name: <Nav.Link as={NavLink} to="/products/ksm001" exact>Formula of Love Album</Nav.Link>,
		        description: "Nostrud velit dolor excepteur ullamco consectetur aliquip tempor. Consectetur occaecat laborum exercitation sint reprehenderit irure nulla mollit. Do dolore sint deserunt quis ut sunt ad nulla est consectetur culpa. Est esse dolore nisi consequat nostrud id nostrud sint sint deserunt dolore.",
		        price: 1000,
		        onOffer: true
		    },
		    {
		        id: "ksm002",
		        name: "IM NAYEON (Pre-Order)",
		        description: "Nostrud velit dolor excepteur ullamco consectetur aliquip tempor. Consectetur occaecat laborum exercitation sint reprehenderit irure nulla mollit. Do dolore sint deserunt quis ut sunt ad nulla est consectetur culpa. Est esse dolore nisi consequat nostrud id nostrud sint sint deserunt dolore.",
		        price: 800,
		        onOffer: true
		    },
		    {
		        id: "ksm003",
		        name: "itzy Photocards",
		        description: "Eu non commodo et eu ex incididunt minim aliquip anim. Aliquip voluptate ut velit fugiat laborum. Laborum dolore anim pariatur pariatur commodo minim ut officia mollit ad ipsum ex. Laborum veniam cupidatat veniam minim occaecat veniam deserunt nulla irure. Enim elit sint magna incididunt occaecat in dolor amet dolore consectetur ad mollit. Exercitation sunt occaecat labore irure proident consectetur commodo ad anim ea tempor irure.",
		        price: 500,
		        onOffer: true
		    },
		    {
		        id: "ksm004",
		        name: "GOT7 Photocards",
		        description: "Eu non commodo et eu ex incididunt minim aliquip anim. Aliquip voluptate ut velit fugiat laborum. Laborum dolore anim pariatur pariatur commodo minim ut officia mollit ad ipsum ex. Laborum veniam cupidatat veniam minim occaecat veniam deserunt nulla irure. Enim elit sint magna incididunt occaecat in dolor amet dolore consectetur ad mollit. Exercitation sunt occaecat labore irure proident consectetur commodo ad anim ea tempor irure.",
		        price: 600,
		        onOffer: true
		    },
		    {
		        id: "ksm005",
		        name: "Twice World Tour III Shirt",
		        description: "Proident est adipisicing est deserunt cillum dolore. Fugiat incididunt quis aliquip ut aliquip est mollit officia dolor ea cupidatat velit. Consectetur aute velit aute ipsum quis. Eiusmod dolor exercitation dolor mollit duis velit aliquip dolor proident ex exercitation labore cupidatat. Eu aliquip mollit labore do.",
		        price: 750,
		        onOffer: true
		    }
		]

		export default productsData;