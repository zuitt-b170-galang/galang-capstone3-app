import React ,{ useState, useEffect }from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';
import UserContext from './UserContext.js';


import AppNavbar from './components/AppNavbar';
import Products from './pages/Products';
import Cart from './pages/Cart';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Error from './pages/Error';
import Logout from './pages/Logout';

import './App.css';

    export default function App() {
        const [user, setUser] = useState({
        id: null,
        isAdmin: null
    });

    const unsetUser = () => {
        localStorage.clear();
        setUser( { access: null } )
    }

    useEffect(() => {

        fetch(`https://mighty-stream-66738.herokuapp.com/api/users/details`, {
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {

 
            if (typeof data._id !== "undefined") {
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                });
            } else {
                setUser({
                    id: null,
                    isAdmin: null
                });
            }
        })
    }, []);

        return (
          <UserContext.Provider value={{ user, setUser, unsetUser }}>
             <Router>
               <AppNavbar user={user} />
               <Routes>
                   <Route path = "/" element={<Home />} />
                   <Route path = "/products" element = {<Products />} />
                   <Route path = "/cart" element = {<Cart />} />
                   <Route path = "/register" element = {<Register />} />
                   <Route path = "/login" element = {<Login />} />
                   <Route path = "/logout" element = {<Logout />} />

                   <Route path="*" element = {<Error />} />
               </Routes>
            </Router>
        </UserContext.Provider>
        )
}